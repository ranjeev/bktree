#include <stdlib.h>
#include <stdio.h>
#include <glib.h>
#include <glib/gstdio.h>        // for g_remove
#include "bktree.h"
#include "editdistance.h"

typedef struct {
    bktree_s *tree;
} fixture;



void tree_setup(fixture *tf, gconstpointer test_data)
{
    int *db_code = (int*)test_data;
    tf->tree = bk_load(distance, *db_code);
}



void tree_teardown(fixture *tf, gconstpointer test_data)
{
    bk_destroy(tf->tree);
}



unsigned long count_lines_of_file(const char *file_name) {
    FILE* myfile = fopen(file_name, "r");
    int ch;
    unsigned long lines = 0;

    do{
        ch = fgetc(myfile);
        if(ch == '\n')
            lines++;
    }while (ch != EOF);

    if (ch != '\n' && lines > 0)
        lines++;

    fclose(myfile);
    return lines;
}



void test_create(gconstpointer test_data)
{
    int *db_code = (int*)test_data;

    // Remove the database file.
    char *file;
    asprintf(&file, "%s/%d", DATADIR, *db_code);
    g_remove(file);
    free(file);
        
    const char *file_name = "words";
    int lines = count_lines_of_file(file_name);
    
    int added_words;
    added_words = bk_create(distance, *db_code, file_name);
    
    g_assert_cmpint(lines, ==, added_words);
}



void test_delete(gconstpointer test_data)
{
    int *db_code = (int*)test_data;
    bktree_s *tree = bk_load_write(distance, *db_code);
    
    const char *word = "compile";
    const char *QUERY_WORD = "compile";
    const int max_dist = 1;

    word_list *result, *tmp;
    struct list_head *pos, *q;
    int count_prev = 0;

    result = bk_query(tree, QUERY_WORD, max_dist);
    
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++count_prev;
    }
    free(result);

    
    g_assert_cmpint(bk_delete(tree, word), ==, true);

    int count_after = 0;

    result = bk_query(tree, QUERY_WORD, max_dist);
    
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++count_after;
    }
    free(result);
    
    ++count_after;      // Increment it once more for the delete.
    
    g_assert_cmpint(count_prev, ==, count_after);

    bk_destroy(tree);
}



void test_add(gconstpointer test_data)
{
    int *db_code = (int*)test_data;
    bktree_s *tree = bk_load_write(distance, *db_code);
    
    const char *QUERY_WORD = "compile";
    const int max_dist = 1;
    word_list *result, *tmp;
    struct list_head *pos, *q;
    int count_prev = 0;

    result = bk_query(tree, QUERY_WORD, max_dist);
    
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++count_prev;
    }
    free(result);
    ++count_prev;       // Increment it once more for the next addition.
    
    g_assert_cmpint(bk_add_word(tree, QUERY_WORD, NULL), ==, true);

    int count_after = 0;

    result = bk_query(tree, QUERY_WORD, max_dist);
    
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++count_after;
    }
    free(result);
    
    g_assert_cmpint(count_prev, ==, count_after);

    bk_destroy(tree);
}



void test_cm_create()
{   
    // Remove the database file.
    char *file;
    asprintf(&file, "%s/%s", DATADIR, CM_DB_NAME);
    g_remove(file);
    free(file);
    
    const char *file_name = "cm";
    int lines = count_lines_of_file(file_name);
    
    g_assert_cmpint(cm_create(file_name), ==, lines);
}



void test_cm_query()
{
    cm_s *cm = cm_init(CM_READ);
    const double KNOWN_PROB = 0.6;
    g_assert_cmpfloat(cm_get(cm, "1L"), ==, KNOWN_PROB);
    cm_destroy(cm);
}



void test_best_match(fixture *tf, gconstpointer data)
{
    char *match = NULL;
    match = bk_best_match(tf->tree, "ostracise", 2, NULL);
    g_assert_cmpstr(match, ==, "OSTRACINE");
    free(match);
}



void test_query(fixture *tf, gconstpointer data)
{   
    const char *QUERY_WORD = "accomplish";
    const int KNOWN_COUNT = 6;
    const int max_dist = 2;

    word_list *result, *tmp;
    struct list_head *pos, *q;
    int count = 0;

    result = bk_query(tf->tree, QUERY_WORD, max_dist);
    
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++count;
    }
    free(result);
    
    
    g_assert_cmpint(count, ==, KNOWN_COUNT);
}



void test_get_max_length(fixture *tf, gconstpointer ignored)    
{
    // The maximum length of a word in the English dictionary is 24.
    int known_max_length = 24;
    int max_length = bk_get_max_length(tf->tree);
    
    g_assert_cmpint(max_length, ==, known_max_length);
}



void test_get_freq(fixture *tf, gconstpointer ignored)
{
    /* In the dictionary file, word, that is provided with the tests, I have
     * added a frequency of 235 for the word zoosmosis.*/
    const unsigned int known_freq = 235;
    g_assert_cmpint(bk_get_freq(tf->tree, "zoosmosis"), ==, known_freq);
}



int main(int argc, char **argv)
{
    const int db_code = 9999;
    
    g_test_init(&argc, &argv, NULL);
    
    g_test_add_func("/create/cm", test_cm_create);
    
    g_test_add_func("/query/cm", test_cm_query);
    
    g_test_add_data_func("/create/db", &db_code, test_create);
    
    g_test_add_data_func("/create/delete", &db_code, test_delete);
    
    g_test_add_data_func("/create/add", &db_code, test_add);
    
    g_test_add("/query/best", fixture, &db_code,
               tree_setup, test_best_match, tree_teardown);
               
    g_test_add("/query/query", fixture, &db_code,
               tree_setup, test_query, tree_teardown);
               
    g_test_add("/query/max_length", fixture, &db_code,
               tree_setup, test_get_max_length, tree_teardown);
               
    g_test_add("/query/freq", fixture, &db_code,
               tree_setup, test_get_max_length, test_get_freq);
               
    
    return g_test_run();
}
