#include <Python.h>
#include <string.h>
#include <stdbool.h>
#include "bktree.h"
#include "editdistance.h"


typedef struct {
    PyObject_HEAD
    bktree_s *tree;
    int access;
} BKTree;


// Checks if the BKTree object was loaded with write access.
static inline PyObject *check_for_write(PyObject *self){
    if (((BKTree*)self)->access != 1){
        PyErr_SetString(PyExc_IOError, 
                        "You must open the tree with write access.");
        return NULL;
    }
    return Py_True;
}


// The __new__ function 
static PyObject *BKTree_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    BKTree *self;
    self = (BKTree *)type->tp_alloc(type, 0);
    return (PyObject *)self;
}


/**
 * The __init__ function.
 * @param dbcode a unique id identifying the tree.
 * @param access an optional string r|w specifying the access to the tree.
 * @param alpha, beta_dist, beta_freq, beta_cm the parameters to generate the 
 * score.
 * Defaults to r, that is read access. */
static int BKTree_init(BKTree *self, PyObject *args, PyObject *kwds)
{
    int dbcode;
    const char *READ = "r";
    const char *WRITE = "w";
    const char *access = NULL;

    double alpha = NAN;
    double beta_dist = NAN; 
    double beta_freq = NAN;
    double beta_cm = NAN;
    
    static char *kwlist[] = {"dbcode", "access", "alpha", 
                             "beta_dist", "beta_freq", "beta_cm", NULL};
    
    if (!PyArg_ParseTupleAndKeywords(args, kwds, "i|sdddd", kwlist,
                                     &dbcode, &access,
                                     &alpha, &beta_dist, 
                                     &beta_freq, &beta_cm)){
        return -1;  
    }

    if (access == NULL || strcmp(access, READ) == 0){
        self->tree = bk_load(distance, dbcode);
        self->access = 0;
    }else if (strcmp(access, WRITE) == 0){
        self->tree = bk_load_write(distance, dbcode);
        self->access = 1;
    }
        
    if (self->tree == NULL){
        PyErr_SetString(PyExc_IOError, "Could not load the tree.");
        return -1;
    }
    
    if (alpha != NAN && beta_dist != NAN && beta_freq != NAN && beta_cm != NAN)
        bk_set_params(self->tree, alpha, beta_dist, beta_freq, beta_cm); 
    
    return 0;
}


// The default destructor
static void BKTree_dealloc(BKTree *self)
{
    if(self->tree != NULL)
        bk_destroy(self->tree);
    self->ob_type->tp_free((PyObject*)self);
}


/**
 * Returns the best match for a given word and its score 
 * @param word the word to query for.
 * @param max_dist maximum distance the word can be from a word in the tree. 
 * If max_dist is not provided max_dist is assumed to be length(word)/2. */
static PyObject *BKTree_best(PyObject *self, PyObject *args, PyObject *kwds)
{
    int max_dist = -1;
    char *result = NULL;
    const char *word = NULL;
    double word_score = 0.0;
    
    if (!PyArg_ParseTuple(args, "s|i", &word, &max_dist)) 
        return NULL;
        
    if (max_dist < 0)
        max_dist = strlen(word) / 2;
    
    result = bk_best_match(((BKTree*)self)->tree, word, max_dist, &word_score);
        
    return Py_BuildValue("sf", result, word_score);
}


/**
 * Returns a list of similar words with their scores
 * @param word the word to query for.
 * @param max_dist maximum distance the word can be from a word in the tree. 
 * If max_dist is not provided max_dist is assumed to be length(word)/2.
 * @param ini_score an initial score can be specified to adjust the scores
 * returned by the function. */
static PyObject *BKTreequery(PyObject *self, PyObject *args)
{
    int max_dist = -1;
    double ini_score = 0.;
    const char *word = NULL;
    
    if (!PyArg_ParseTuple(args, "s|id", &word, &max_dist, &ini_score)) 
        return NULL;
    
    if (max_dist < 0)
        max_dist = strlen(word) / 2;
    
    word_list *result = NULL;
    
    result = bk_query(((BKTree*)self)->tree, word, max_dist);
    
    
    word_list *tmp;
    struct list_head *pos, *q;
    PyObject *tpl;
    PyObject *list = PyList_New(0);

    result = bk_query(((BKTree*)self)->tree, word, max_dist);

    int i = 0;
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        tpl = Py_BuildValue("sf", tmp->word, ini_score + tmp->score);
        PyList_Append(list, tpl);
        
        free(tmp->word);
        list_del(pos);
        free(tmp);
        ++i;
    }
    free(result);
        
    return list;
}


// Adds a word to the tree
static PyObject *BKTreeadd(PyObject *self, PyObject *args)
{
    if (check_for_write(self) != Py_True)
        return NULL;
    
    const char *word = NULL;
    unsigned int freq = 1;

    if (!PyArg_ParseTuple(args, "s|i", &word, &freq))
        return NULL;
    
    if (bk_add_word(((BKTree*)self)->tree, word, &freq))
        return Py_True;
    else
        return Py_False;
}


// Deletes a word from the tree
static PyObject *BKTreedelete(PyObject *self, PyObject *args)
{
    if (check_for_write(self) != Py_True)
        return NULL;

    const char *word = NULL;

    if (!PyArg_ParseTuple(args, "s", &word))
        return NULL;
    
    if (bk_delete(((BKTree*)self)->tree, word))
        return Py_True;
    else
        return Py_False;
}


// Creates a tree from a given file
static PyObject *create_tree(PyObject *self, PyObject *args)
{    
    int dbcode;
    const char *filename = NULL;

    if (!PyArg_ParseTuple(args, "is", &dbcode, &filename))
        return NULL;
        
    unsigned long response = bk_create(distance, dbcode, filename);
    
    return Py_BuildValue("l", response);
}


static PyMethodDef BKTree_methods[] = {
    {"best", (PyCFunction)BKTree_best, METH_VARARGS,
     "best(word, max_dist=len(word)/2)\n"
         "Returns a tuple of best match and its score"
             " for a given word and its score."},
     
    {"query", (PyCFunction)BKTreequery, METH_VARARGS,
     "query(word, max_dist=len(word)/2, ini_score=0)\n"
         "Returns a list tuple of words and score that are"
             " atmost max_dist from given word."},
     
    {"add", (PyCFunction)BKTreeadd, METH_VARARGS,
     "add(word, freq=1)\n"
         "Add the given word to the tree."},
     
    {"delete", (PyCFunction)BKTreedelete, METH_VARARGS,
     "delete(word)\n"
         "Deletes the given word from the tree."},
     
    {NULL}
};



static PyTypeObject BKTreeType = {
    PyObject_HEAD_INIT(NULL)
    0,                                          /*ob_size*/
    "pybktree.BKTree",                          /*tp_name*/
    sizeof(BKTree),                             /*tp_basicsize*/
    0,                                          /*tp_itemsize*/
    (destructor)BKTree_dealloc,                 /*tp_dealloc*/
    0,                                          /*tp_print*/
    0,                                          /*tp_getattr*/
    0,                                          /*tp_setattr*/
    0,                                          /*tp_compare*/
    0,                                          /*tp_repr*/
    0,                                          /*tp_as_number*/
    0,                                          /*tp_as_sequence*/
    0,                                          /*tp_as_mapping*/
    0,                                          /*tp_hash */
    0,                                          /*tp_call*/
    0,                                          /*tp_str*/
    0,                                          /*tp_getattro*/
    0,                                          /*tp_setattro*/
    0,                                          /*tp_as_buffer*/
    Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,   /*tp_flags*/
    "BKTree(dbcode, access='r')\n"          
    "   dbcode specifies the database file\n"
    "   access can be 'r' or 'w', specifying read or write access.",  /* tp_doc */
    0,                                          /* tp_traverse */
    0,                                          /* tp_clear */
    0,                                          /* tp_richcompare */
    0,                                          /* tp_weaklistoffset */
    0,                                          /* tp_iter */
    0,                                          /* tp_iternext */
    BKTree_methods,                             /* tp_methods */
    0,                                          /* tp_members */
    0,                                          /* tp_getset */
    0,                                          /* tp_base */
    0,                                          /* tp_dict */
    0,                                          /* tp_descr_get */
    0,                                          /* tp_descr_set */
    0,                                          /* tp_dictoffset */
    (initproc)BKTree_init,                      /* tp_init */
    0,                                          /* tp_alloc */
    BKTree_new,                                 /* tp_new */
};



static PyMethodDef module_methods[] = {
    {"create_tree", (PyCFunction)create_tree, METH_VARARGS,
     "create_tree(dbcode, filename)\n"
         "Creates a tree from the given filename. File should contain words"
             " in each line with frequency specified after the word."},
    {NULL}  /* Sentinel */
};



#ifndef PyMODINIT_FUNC  /* declarations for DLL import/export */
#define PyMODINIT_FUNC void
#endif


PyMODINIT_FUNC initpybktree(void){
    
    PyObject* m;

    if (PyType_Ready(&BKTreeType) < 0)
        return;
    
    const char *doc_string = \
        "A BK Tree implementation. \n"
        "create_tree(dbcode, filename): Creates the tree in the filesystem.\n"
        "BKTree(dbcode, access='r', alpha=0.03342, beta_dist=-0.0165, beta_freq=0.000000117, beta_cm=0.0004478): Constructor for the BKTree object.\n"
        "    Methods: \n"
        "    best: Returns a tuple of best match and its score.\n"
        "    query: Returns a list tuple of words and score within"
             " the editdistance provided.\n"
        "    add: Adds a word to the tree.\n"
        "    delete: Deletes a word from the tree.\n"
        "    update_cm: Increase the probability of a character tuple.\n";

    m = Py_InitModule3("pybktree", module_methods, doc_string);

    if (m == NULL)
      return;

    Py_INCREF(&BKTreeType);
    PyModule_AddObject(m, "BKTree", (PyObject *)&BKTreeType);
}
