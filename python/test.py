import unittest
import os
import pybktree


class TestQueryMethods(unittest.TestCase):
    
    def setUp(self):
        self.tree = pybktree.BKTree(9999, "w")
        self.max_dist = 2
        
    def test_query(self):
        known_result_len = 6
        result = self.tree.query("accomplish", self.max_dist)
        self.assertEqual(len(result), known_result_len)
    
    def test_best(self):
        known_result = "OSTRACINE"
        result, _ = self.tree.best("ostracise", self.max_dist)
        self.assertEqual(result, known_result)
    
    def test_add(self):
        ADD_WORD = "accomplishes"
        len_before = len(self.tree.query(ADD_WORD, 0))
        self.assertEqual(len_before, 0)
        self.tree.add(ADD_WORD)
        len_after = len(self.tree.query(ADD_WORD, 0))
        self.assertEqual(len_after, 1)     
    
    def test_delete(self):
        DEL_WORD = "accomplishes"
        len_before = len(self.tree.query(DEL_WORD, 0))
        self.assertEqual(len_before, 1)
        self.tree.delete(DEL_WORD)
        len_after = len(self.tree.query(DEL_WORD, 0))
        self.assertEqual(len_after, 0)     
           
    def tearDown(self):
        del(self.tree)
        
if __name__ == '__main__':
    if (os.path.exists("/usr/local/var/bktree/cm__db") and
            os.path.exists("/usr/local/var/bktree/9999")):
        unittest.main()
    
    else:
        print("Please make sure you have run the C tests first"
                " by executing ./tests")