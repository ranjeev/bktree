#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <stdio.h>

#include "editdistance.h"
#include "bktree.h"

void usage()
{
    fprintf(stderr, "Usage: \n"
            "       --best DB_CODE WORD\n"
            "       --query DB_CODE WORD MAX_DIST\n"
            "       --freq DB_CODE WORD\n"
            "       --create DB_CODE DB_FILE\n"
            "       --add DB_CODE WORD FREQ\n"
            "       --delete DB_CODE WORD\n"
            "       --cmcreate FILE\n"
            "       --cmquery CM_WORD\n");   
}

int main(int argc, char **argv)
{
    int max_dist;
    int db_code;


    if (argc <= 2){
        usage();
        return -1;
    }   

    
    db_code = atoi(argv[2]);
    
    
    if(strcmp(argv[1], "--create") == 0){
        if (bk_create(distance, db_code, argv[3]) == false){
            printf("Error while creating DB.\n");
            exit(-1);
        }
        else{
            printf("Database created.\n");
            bktree_s *tree = bk_load(distance, db_code);
            printf("Maximum length: %d\n", bk_get_max_length(tree));
            printf("CumFreq: %lu\n", bk_cumulative_freq(tree));

            bk_destroy(tree);
        }
        return 0;
        
    }else if (strcmp(argv[1], "--cmcreate") == 0){
        if(cm_create(argv[2])){
            return 0;
        }
    
    }else if (strcmp(argv[1], "--cmquery") == 0){
        double response;
        cm_s *cm = cm_init(CM_READ);
        response = cm_get(cm, argv[2]);
        printf("Char Mat %s: %f\n", argv[2], response);
        cm_destroy(cm);
        return 0;
    
    }else if (strcmp(argv[1], "--add") == 0){
        unsigned int freq;
        freq = atoi(argv[4]);
        bktree_s *tree = bk_load_write(distance, db_code);
        bk_add_word(tree, argv[3], &freq);
        bk_destroy(tree);
        return 0;
    
    }else if (strcmp(argv[1], "--delete") == 0){
        bktree_s *tree = bk_load_write(distance, db_code);
        bk_delete(tree, argv[3]);
        bk_destroy(tree);
        return 0;
    }

        
    bktree_s *tree = bk_load(distance, db_code);
            
    if (strcmp(argv[1], "--best") == 0){
        double score = 0;
        char *result = bk_best_match(tree, argv[3], 3, &score);
        printf("%s %f\n", result, score);
        return 0;
    
    }else if (strcmp(argv[1], "--query") == 0){
        max_dist = atoi(argv[4]);
        
        word_list *result, *tmp;
        struct list_head *pos, *q;

        result = bk_query(tree, argv[3], max_dist);
        
        list_for_each_safe(pos, q, &result->list){
            tmp = list_entry(pos, word_list, list);
            printf("%s\t\t dist: %u\t freq: %lu\t score: %f\n",
                   tmp->word, tmp->dist, tmp->freq, tmp->score);
            free(tmp->word);
            list_del(pos);
            free(tmp);
        }
        free(result);

    
    }else if (strcmp(argv[1], "--freq") == 0){
        printf("Freq: %d\n",
               bk_get_freq(tree, argv[3]));
    
    }else{
        usage();
        return -1;
    }

    bk_destroy(tree);
    return 0;
}
