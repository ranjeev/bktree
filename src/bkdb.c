#include "bkdb.h"
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>



inline char* bk_get_key(const char *key, const int dist)
{
    char *bk_key = NULL;
    asprintf(&bk_key, "%s%s%d", key, BK_KEY_SEP, dist);
    return bk_key;
}


inline bool bk_add(bktree_s *tree, const char *key, const char *word)
{
    return tchdbput2(tree->db, key, word);
}



inline bool bk_add_with_dist(bktree_s *tree, 
                 const char *key, const char *word, const int dist)
{
    char *bk_key = bk_get_key(key, dist);
    bool response = bk_add(tree, bk_key, word);
    free(bk_key);
    return response;
}



inline char* bk_get(bktree_s *tree, const char *key)
{
    return tchdbget2(tree->db, key);
}



inline char* bk_get_with_dist(bktree_s *tree, const char *key, const int dist)
{
    char *bk_key = bk_get_key(key, dist);
    char *response = bk_get(tree, bk_key);
    free(bk_key);
    return response;    
}


inline bool bk_delete_node(bktree_s *tree, const char *key)
{
    return tchdbout2(tree->db, key);
}


inline bool bk_delete_node_dist(bktree_s *tree, const char *key, const int dist)
{
    char *bk_key = bk_get_key(key, dist);
    bool response = bk_delete_node(tree, bk_key);
    free(bk_key);
    return response;   
}
