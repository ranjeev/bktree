#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include "common.h"


inline void free_pair_s(void *pair)
{
    pair_s *p = (pair_s*)pair;
    free(p->word);
    free(p);
}



char* s_to_upper(const char *word)
{
    char *uword;
    asprintf(&uword, "%s", word);
    for (size_t i=0; i<strlen(uword); i++)
        uword[i] = toupper(uword[i]);
    return uword;
}



inline void bk_chomp(char *s)
{
    int end = strlen(s) - 1;
    if (end >=0 && s[end] == '\n')
        s[end] = '\0';
}
