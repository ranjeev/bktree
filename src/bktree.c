#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <glib.h>
#include <unistd.h>
#include <stdio.h>

#include <glib/gstdio.h>        // for g_mkdir_with_parents

#include <sys/types.h>
#include <sys/stat.h>

#include "bktree.h"
#include "bkdb.h"
#include "common.h"

#define BK_READ 0
#define BK_WRITE 1

#define MAX_LENGTH_KEY "__max_length_key__"
#define CUM_FREQ_KEY "__cum_freq_key__"

#define ALPHA 0.03342;
#define BETA_DIST -0.0165; 
#define BETA_FREQ 0.000000117;
#define BETA_CM 0.0004478;


// Stores the maximum length of a value in the tree.
static bool set_max_length(bktree_s *tree, unsigned int length)
{
    return tchdbput(tree->db, MAX_LENGTH_KEY, strlen(MAX_LENGTH_KEY),
                    (const void*)&length, sizeof(length));
}



// Sets the frequency of a word in the tree.
static bool set_freq(bktree_s *tree, const char *word, unsigned int freq)
{
    char *word_key;
    asprintf(&word_key, "%s%s", META_FREQ, word);
    bool status = tchdbput(tree->db, word_key, strlen(word_key),
                           (const void*)&freq, sizeof(freq));
    free(word_key);
    return status;
}



// Adds words to the database.
static bool add_word(bktree_s *tree, const char* root, const char *word,
              unsigned int freq)
{
    char *parent, *reply, *cum_freq_tmp;
    unsigned long cum_freq;
    int dist;
    
    // Set the initial parent to root.
    asprintf(&parent, "%s", root);
    
    // We upper case all words before adding to the database.
    char *uword = s_to_upper(word);

    while(parent != NULL && 
          (dist = tree->distance
                    (parent, strlen(parent), uword, strlen(uword))) != 0){
        
        reply = bk_get_with_dist(tree, parent, dist);
        
        if (reply != NULL){
            free(parent);
            parent = reply;
        }
        else {
            // Add word to the database.
            if (bk_add_with_dist(tree, parent, uword, dist)){
                // Set the frequency and update the cumulative frequency.
                set_freq(tree, uword, freq);
                
                cum_freq = bk_cumulative_freq(tree) + freq;
                asprintf(&cum_freq_tmp, "%lu", cum_freq);
                bk_add(tree, CUM_FREQ_KEY, cum_freq_tmp);
                
                // Update the maximum length of the tree.
                if (strlen(uword) > bk_get_max_length(tree))
                    set_max_length(tree, strlen(uword));
                
                free(cum_freq_tmp);
                free(parent);
            }
            parent = NULL;
        }
    }
    free(uword);
    return true;
}



bool bk_add_word(bktree_s *tree, const char *word, const unsigned int *freq)
{
    char *root = ROOT;
    if (freq == NULL)
        return add_word(tree, root, word, 1);
    else
        return add_word(tree, root, word, *freq);
}



/* Inititalises a bktree_s object with the given distance function and db_code.
 * If access_code is set to BK_READ it initialises the tree for read access 
 * otherwise for write access if it is set to BK_WRITE.*/
static bktree_s* init_tree(distance_fn distance, int db_code, int access_code)
{
    TCHDB *db;
    int ecode;
    char *DATABASE;
    
    // Create the base directory if it does not exist.
    if (g_mkdir_with_parents(DATADIR, 0770) < 0){
        printf("Could not access the base directory: %s. "
               "Please make sure it exists or you have the "
               "appropriate permissions.", DATADIR);
        return NULL;
    }
    
    asprintf(&DATABASE, "%s/%d", DATADIR, db_code);

    db = tchdbnew();

    // Exit if threading is not supported.
    if (!tchdbsetmutex(db)){
        fprintf(stderr, "Sorry threading is not supported!\n");
        return NULL;
    }
    
    bktree_s *tree = malloc(sizeof(bktree_s));
    
    tree->distance = distance;
    
    if (access_code == BK_READ){
        // Open tree for read access.
        if(!tchdbopen(db, DATABASE, HDBOREADER)){
            ecode = tchdbecode(db);
            fprintf(stderr, "open error: %s\n", tchdberrmsg(ecode));
            return NULL;
        }
        tree->db = db;
    }
    else{
        // Open tree for write access. If file does not exist create it.
        if(!tchdbopen(db, DATABASE, HDBOWRITER|HDBOCREAT)){
            ecode = tchdbecode(db);
            fprintf(stderr, "Open error: %s\n", tchdberrmsg(ecode));
            return NULL;
        }
        tree->db = db;
    }
    free(DATABASE);
    
    // Initialize the parameters for the Tree
    tree->alpha = ALPHA;
    tree->beta_dist = BETA_DIST; 
    tree->beta_freq = BETA_FREQ;
    tree->beta_cm = BETA_CM;
    
    return tree;
}



unsigned long bk_create(distance_fn function, int db_code, 
                        const char *file_name)
{
    bktree_s *tree = init_tree(function, db_code, BK_WRITE);
    
    if (tree == NULL)
        return -1;
    
    /* Get values from the file and adds to db */
    FILE *fp;
    fp = fopen(file_name, "r");
    char *word = NULL;
    char *tmp = NULL;
    int freq;
    size_t linelen = 0;
    int linesize;
    unsigned long no_of_words = 0;
    
    const char *sep = " ";
    const char *root = ROOT;
    
    while ((linesize = getline(&word, &linelen, fp)) > 0){
        bk_chomp(word);
        strtok_r(word, sep, &tmp);
        if (tmp != NULL && *tmp != '\0' ){
            freq = atoi(tmp);
        }
        else{
            freq = 1;
        }
        if(add_word(tree, root, word, freq)){
            ++no_of_words;
        }
    }
    bk_destroy(tree);

    return no_of_words;
}



inline bktree_s* bk_load(distance_fn function, int db_code){
    return init_tree(function, db_code, BK_READ);
}



inline bktree_s* bk_load_write(distance_fn function, int db_code){
    return init_tree(function, db_code, BK_WRITE);
}



unsigned int bk_get_max_length(bktree_s *tree)
{
    int size;
    unsigned int length;
    const char *key = MAX_LENGTH_KEY;
    unsigned int *response;
    response = tchdbget(tree->db, (const void*)key, strlen(key), &size);
    length = (response == NULL) ? 0 : *response;
    free(response);
    return length;
}



unsigned long bk_cumulative_freq(bktree_s *tree)
{
    char *cum_freq_s = bk_get(tree, CUM_FREQ_KEY);
    if (cum_freq_s == NULL)
        return 0;
    else{
        unsigned long cum_freq = atol(cum_freq_s);
        free(cum_freq_s);
        return cum_freq;
    }
}



/* Returns the parent of a given word. It recursively iterates through the tree
 * to get to the word and returns its parent. */
static char* get_parent_node(bktree_s *tree, const char *word)
{
    GAsyncQueue *parent_q = g_async_queue_new();
    int dist, c_dist;
    char *parent, *child_query;

    asprintf(&parent, "%s", ROOT);
    
    // Adding the first parent to it.
    g_async_queue_push(parent_q, parent);
    
    
    while((parent=g_async_queue_try_pop(parent_q)) != NULL){
        dist = tree->distance(parent, strlen(parent), word, strlen(word));
    
        for (int i=0;i <= dist; i++){
            child_query = bk_get_with_dist(tree, parent, i);
            
            if (child_query != NULL){
                c_dist = tree->distance(child_query, strlen(child_query), 
                                        word, strlen(word));
                if(c_dist == 0){
                    // Free up the rest of items in the queue;
                    char *temp;
                    while ((temp = g_async_queue_try_pop(parent_q)) != NULL)
                        free(temp);
                    free(child_query);
                    
                    return parent;              
                }
                g_async_queue_push(parent_q, child_query);
            }
        }
        free(parent);
    }
    return NULL;
}



/* Returns all the children of a given word. It returns a GHashTable with the
 * key as the distance of the child form the word and value the child itself.*/
static GHashTable* get_all_children(bktree_s *tree, const char *word)
{
    GHashTable *children = g_hash_table_new_full(g_int_hash, g_int_equal,
                                                 free, free); 
    int max_length = bk_get_max_length(tree);
    char *child;
    int *dist;
    
    /* Look for words with distance that is utmost the maximum length of any
     * word in the whole tree. */
    for (int i=1; i<=max_length; i++){
        child = bk_get_with_dist(tree, word, i);
        if (child != NULL){
            dist = (int*)malloc(sizeof(int));
            *dist = i;
            g_hash_table_insert(children, dist, child);
        }
    }
    return children;
}



bool bk_delete(bktree_s *tree, const char *word)
{
    char *parent;
    char *uword = s_to_upper(word);
    parent = get_parent_node(tree, uword);

    // Return false if we can't find a parent.
    if (parent == NULL) return false;

    // Find uword with the same distance from the parent.
    int dist = tree->distance(parent, strlen(parent),
                              uword, strlen(uword));

    GHashTable *children = get_all_children(tree, uword);

    bk_delete_node_dist(tree, parent, dist);

    if (g_hash_table_size(children) == 0){
        return true;
    }
    
    GHashTableIter iter;
    void *key, *value;

    g_hash_table_iter_init(&iter, children);
    while (g_hash_table_iter_next(&iter, &key, &value)){
        bk_delete_node_dist(tree, uword, *((int*)key));
        dist = tree->distance(parent, strlen(parent),
                              (char*)value, strlen((char*)value));
        char *child_key = bk_get_key(parent, dist);
        add_word(tree, parent, (char*)value, bk_get_freq(tree, (char*)value));
        free(child_key);
    }
    return true;
}



void bk_destroy(bktree_s* tree)
{
    int ecode;
    
    /* close the database */
    if(!tchdbclose(tree->db)){
        ecode = tchdbecode(tree->db);
        fprintf(stderr, "close error: %s\n", tchdberrmsg(ecode));
    }
    tchdbdel(tree->db);

    free(tree);

    tree = NULL;
}



void bk_set_params(bktree_s *tree, const double alpha, 
                   const double beta_dist, const double beta_freq,
                   const double beta_cm)
{
    tree->alpha = alpha;
    tree->beta_dist = beta_dist;
    tree->beta_freq = beta_freq;
    tree->beta_cm = beta_cm;
}