#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <glib/gstdio.h>        // for g_mkdir_with_parents

#include "bktree.h"
#include "bkdb.h"
#include "common.h"



/* Loads the Character Matrix database. If there the database does not exist
 * a new one is created. Returns NULL if any problem arises. */
cm_s* cm_init(int access_code)
{
    TCHDB *db;
    int ecode;
    char *DATABASE;
    
    // Create the base directory if it does not exist.
    if (g_mkdir_with_parents(DATADIR, 0770) < 0){
        printf("Could not access the base directory: %s. "
               "Please make sure it exists or you have the "
               "appropriate permissions.", DATADIR);
        return NULL;
    }
    
    asprintf(&DATABASE, DATADIR "/" CM_DB_NAME);

    db = tchdbnew();

    // Exit if threading is not supported.
    if (!tchdbsetmutex(db)){
        printf("Sorry threading is not supported!\n");
        exit(-1);
    }
    tchdbsetcache(db, 200);
    cm_s *cm = malloc(sizeof(cm_s));
    
    /* Open a reader if file_name is not specified. If specified
     * lets create a new database using the file_name. */
    if (access_code == CM_READ){
        if(!tchdbopen(db, DATABASE, HDBOREADER)){
            ecode = tchdbecode(db);
            fprintf(stderr, "open error in reading: %s\n", tchdberrmsg(ecode));
            return NULL;
        }
        cm->db = db;
    }
    else if (access_code == CM_WRITE){
        if(!tchdbopen(db, DATABASE, HDBOWRITER|HDBOCREAT)){
            ecode = tchdbecode(db);
            fprintf(stderr, "Open error in writing: %s\n", tchdberrmsg(ecode));
            return NULL;
        }
        cm->db = db;
    }
    free(DATABASE);
    return cm;
}



/* Create the Character Matrix database or update the existing values. It is
 * responsibility of the user to pass the appropriate values. The code does
 * not implement the logic for the values. */
unsigned int cm_create(const char *file_name)
{
    cm_s *cm = cm_init(CM_WRITE);
    if (cm == NULL)
        return false;
    /* Get values from the file and adds to db */
    FILE *fp;
    fp = fopen(file_name, "r");
    char *word = NULL;
    char *tmp = NULL;
    double freq;
    size_t linelen = 0;
    int linesize;
    unsigned int count = 0;
    
    const char *sep = " ";
    
    
    while ((linesize = getline(&word, &linelen, fp)) > 0){
        bk_chomp(word);
        strtok_r(word, sep, &tmp);
        freq = atof(tmp);

        if (tchdbput(cm->db, word, strlen(word),
                     (const void*)&freq, sizeof(freq))){
            ++count;
        }
        else
            printf("Not added %s", word);
    }
    cm_destroy(cm);

    return count;
}



double cm_get(cm_s *cm, const char *key)
{
    int size;
    double ret_val;
    double *response;
    response =  tchdbget(cm->db, (const void*)key, strlen(key), &size);
    ret_val = (response == NULL) ? 0.0 : *response;
    free(response);
    return ret_val;
}



void cm_destroy(cm_s *cm)
{
    int ecode;
    
    /* close the database */
    if(!tchdbclose(cm->db)){
        ecode = tchdbecode(cm->db);
        fprintf(stderr, "close error: %s\n", tchdberrmsg(ecode));
    }
    tchdbdel(cm->db);

    free(cm);

    cm = NULL;
}