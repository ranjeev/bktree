#include <ctype.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <glib.h>
#include <unistd.h>
#include <stdio.h>
#include "bktree.h"
#include "bkdb.h"
#include "common.h"



/* Structure to pass data to the query function. This would have been
 * unnecessary if I was not using threads */
typedef struct query_data{
    const char *word;
    int max_dist;
    bktree_s *tree;
    word_list *result;
    GAsyncQueue *parent_q;
} query_data;



unsigned int bk_get_freq(bktree_s *tree, const char *word)
{
    int size;
    unsigned int freq;
    unsigned int *response;
    char *key;
    char *uword = s_to_upper(word);
    asprintf(&key, "%s%s", META_FREQ, uword);
    response = tchdbget(tree->db, (const void*)key, strlen(key), &size);
    freq = (response == NULL) ? 0 : *response;
    free(uword);
    free(key);
    free(response);
    return freq;
}


/* Returns a score in the range of 0 - 1. */
static double calc_score(bktree_s *tree, cm_s *cm, 
                  const char* o_word, const char *word, 
                  const int dist, 
                  const unsigned long freq, const unsigned long total)
{
    double cm_score;
    char key[3];

    size_t len = strlen(word);

    if (strlen(o_word) != len)
        cm_score = 0.0;
    else{
        double sum = 0;
        for (unsigned int i=0; i<len; i++){
            key[0] = o_word[i];
            key[1] = word[i];
            key[2] = '\0';
            sum += cm_get(cm, key);
        }
        cm_score = sum / len;
    }

    return tree->alpha + tree->beta_dist * dist 
                       + tree->beta_freq * freq 
                       + tree->beta_cm * cm_score;
}




/* Pushes words that are of an edit distance of at most max_dist into the 
 * result_q, both of which are members of the query_data.*/
static void *get_word(void *data)
{
    query_data *args = (query_data *)data;
    static GMutex mutex;
    
    int i, dist;
    char *parent = NULL;
    char *child = NULL;
    word_list *tmp;

    while((parent = g_async_queue_try_pop(args->parent_q)) != NULL){
        
        dist = args->tree->distance(parent, strlen(parent),
                                    args->word, strlen(args->word));

        i = dist - args->max_dist;
        if (dist <= args->max_dist){
            tmp = (word_list*)malloc(sizeof(word_list));
            tmp->word = parent;
            tmp->dist = dist;
            g_mutex_lock(&mutex);
            list_add(&tmp->list, &args->result->list);
            g_mutex_unlock(&mutex);
            i = 1;
        }

        for (;i <= dist + args->max_dist; i++){
            child = bk_get_with_dist(args->tree, parent, i);
            if (child != NULL)
                g_async_queue_push(args->parent_q, child);
        }
        
        if (dist > args->max_dist)
            free(parent);
    }
    return NULL;
}



/* Start threads for querying the words.*/
static void query_tree_in_parallel(bktree_s *tree,
                            word_list *result,
                            const char *word,
                            const int m_dist, const int MAXTHREADS)
{
    GThreadPool *thread_pool = NULL;

    GAsyncQueue *parent_q = g_async_queue_new();
    
    query_data *data_s = (query_data*) malloc(sizeof(query_data));

    thread_pool = g_thread_pool_new((GFunc) get_word, data_s, MAXTHREADS,
                                    true, NULL);
    
    // Doing this extra step so that one can free any item from the parent_q.
    char *root;
    asprintf(&root, "%s", ROOT);
    g_async_queue_push(parent_q, root);

    char *uword = s_to_upper(word);

    query_data data ={.tree = tree,
                        .word = uword,
                        .max_dist = m_dist,
                        .result = result,
                        .parent_q = parent_q};

    while (g_async_queue_length(parent_q) != 0) {
        g_thread_pool_push(thread_pool, (void*)&data, NULL);
        usleep(10);
    }

    g_thread_pool_free(thread_pool, FALSE, true);
    free(uword);
    free(data_s);
    free(parent_q);
}



/* Returns the total of all frequencies in the word_list. It also updates the
 * freq attribute of the object.*/
static unsigned long fetch_frequencies(bktree_s *tree, word_list *result)
{
    word_list *tmp;
    struct list_head *pos;
    unsigned long total = 0;
    
    list_for_each(pos, &result->list){
        tmp = list_entry(pos, word_list, list);
        tmp->freq = bk_get_freq(tree, tmp->word);
        total += tmp->freq;
    }
    
    return total;
}



word_list* bk_query(bktree_s *tree, const char *word, int max_dist)
{
    const long MAXTHREADS = sysconf(_SC_NPROCESSORS_ONLN);

    word_list *result = (word_list*)malloc(sizeof(word_list));
    INIT_LIST_HEAD(&result->list);

    query_tree_in_parallel(tree, result, word, max_dist, MAXTHREADS);
    
    unsigned long total = fetch_frequencies(tree, result);
    
    cm_s *cm = cm_init(CM_READ);
    word_list *tmp;
    struct list_head *pos;
    
    list_for_each(pos, &result->list){
        tmp = list_entry(pos, word_list, list);
        tmp->score = calc_score(tree, cm, word,
                                tmp->word, tmp->dist, 
                                tmp->freq, total);
    }
    
    cm_destroy(cm);
    
    return result;
}



char* bk_best_match(bktree_s *tree, const char *word,
                    const int max_dist, double *word_score)
{
    const long MAXTHREADS = sysconf(_SC_NPROCESSORS_ONLN);
    int m_dist = 0;
    double score = -1.0;

    word_list *result = (word_list*)malloc(sizeof(word_list));
    INIT_LIST_HEAD(&result->list);

    do{
        query_tree_in_parallel(tree, result, word, m_dist, MAXTHREADS);
    } while(list_empty(&result->list) && ++m_dist <= max_dist);


    char *response = NULL;
    word_list *tmp;
    struct list_head *pos, *q;
    unsigned long total = fetch_frequencies(tree, result);
    cm_s *cm = cm_init(CM_READ);
    
    list_for_each(pos, &result->list){
        tmp = list_entry(pos, word_list, list);
        tmp->score = calc_score(tree, cm, word,
                                tmp->word, tmp->dist, 
                                tmp->freq, total);
        if (tmp->score > score){
            score = tmp->score;
            response = tmp->word;
        }
    }
    
    /* Copy the result so that destroying the word_list does not destroy the
    * string. */
    if (response != NULL)
        asprintf(&response, "%s", response);
    
    if (word_score != NULL)
        *word_score = score;
    
    // Free up the space used by the list.
    list_for_each_safe(pos, q, &result->list){
        tmp = list_entry(pos, word_list, list);
        free(tmp->word);
        list_del(pos);
        free(tmp);
    }
    free(result);
    
    cm_destroy(cm);
    
    return response;
}
