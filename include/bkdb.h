/* Common functions to the DB. */
#ifndef BK_DB_H
#define BK_DB_H 1


#include "bktree.h"

// Key for storing frequency of a word.
#define META_FREQ "meta:freq:"

#define BK_KEY_SEP "__"



/* Given a word and distance return the key that is used to store the node. */
char* bk_get_key(const char *key, const int dist);

/* Add word to the Tree. Returns true if successful. */
bool bk_add(bktree_s *tree, const char *key, const char *word);

/* Add word to the Tree given the distance and key. */
bool bk_add_with_dist(bktree_s *tree, 
                      const char *key, const char *word, const int dist);

/* Returns the value for a given key. Returns NULL if absent. */
char* bk_get(bktree_s *tree, const char *key);

/* Returns the value for a given key with distance. */
char* bk_get_with_dist(bktree_s *tree, const char *key, const int dist);

/* Deletes a key from the db. */
bool bk_delete_node(bktree_s *tree, const char *key);

/* Deletes a key from the db with distance. */
bool bk_delete_node_dist(bktree_s *tree, const char *key, const int dist);

#endif
