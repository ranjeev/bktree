/* A BK Tree implementation using Tokyo Cabinet. */
#ifdef __cplusplus
extern "C" {
#endif

#ifndef BKTREE_H
#define BKTREE_H 1

#include <tcutil.h>
#include <tchdb.h>
#include "list.h"

/* The root node of the BK Tree. Any ASCII character can be used. */
#define ROOT " "

/* The root directory for all the databases. */
#define DATADIR "/usr/local/var/bktree"



/* The distance function used to create and query a BK Tree. It should
 * be a metric, that is, one that measures similarity or dissimilarity between
 * two strings. */
typedef int (*distance_fn) (const char*, int, const char*, int);



/* Private data struture of BKTree. */
typedef struct bktree_s {
    distance_fn distance;
    TCHDB *db;
    double alpha;
    double beta_dist; 
    double beta_freq;
    double beta_cm;
} bktree_s;



/* A Linked List which contains words that are returned by the bk_query method.
 * The linked list is taken from the Linux Kernel. */
typedef struct word_list{
	char *word;
	double score;
	unsigned int dist;
	unsigned long freq;
	
	struct list_head list;
} word_list;



/**
 * Creates a tree using the words given in a file.
 * @param function a distance function of type distance_fn.
 * @param db_code a unique id identifying the tree.
 * @param file_name the file containing the words. The file should contain only
 * one word in each line. An optional frequency can be provided with the word
 * separated by a space.
 * @return the number of words added to the tree. */
unsigned long bk_create(distance_fn function, int db_code, 
                        const char *file_name);



/**
 * Loads the tree for the given db_code for read access.
 * @param function a distance function of type distance_fn. This must be the
 * same function provided while creating the tree.
 * @param db_code a unique id identifying the tree.
 * @return the bktree_s object or NULL on failure. */
bktree_s* bk_load(distance_fn function, int db_code);



/**
 * Loads the tree for the given db_code for write access. This method creates
 * the database file if it is missing.
 * @param function a distance function of type distance_fn. This must be the
 * same function provided while creating the tree.
 * @param db_code a unique id identifying the tree.
 * @return the bktree_s object or NULL on failure. */
bktree_s* bk_load_write(distance_fn function, int db_code);



/**
 * Closes the connection to the tree.
 * @param tree a bktree_s object. */
void bk_destroy(bktree_s *tree);



/**
 * Returns the maximum length of a word in the given tree.
 * @param tree a bktree_s object. */
unsigned int bk_get_max_length(bktree_s *tree);



/**
 * Returns a list of words with an edit distance of at most max_dist.
 * @param tree a bktree_s object.
 * @param word the word to query for.
 * @param max_dist maximum distance the word can be from a word in the tree. */
word_list* bk_query(bktree_s *tree, const char *word, int max_dist);



/**
 * Returns the best match from the tree for the given word.
 * @param tree a bktree_s object.
 * @param word the word to query for.
 * @param max_dist maximum distance the word can be from a word in the tree. 
 * @param word_score a pointer to store the score of the best match. A NULL
 * value can be passed if you want to ignore it. */
char* bk_best_match(bktree_s *tree, const char *word,
                    const int max_dist, double *word_score);



/**
 * Set the parameters to calculate the best score.
 * @param alpha, beta_dist, beta_freq, beta_cm the parameters for calculating
 * the scores. */
void bk_set_params(bktree_s *tree, const double alpha, 
                   const double beta_dist, const double beta_freq,
                   const double beta_cm);                 

/**
 * Returns the frequency of a given word. If word is not found in the tree
 * 0 is returned.
 * @param tree a bktree_s object.
 * @param word the word to query for. */
unsigned int bk_get_freq(bktree_s *tree, const char *word);



/**
 * Returns the cumulative frequency of all the words in the tree. Returns 0 if
 * the tree is empty.
 * @param tree a bktree_s object. */
unsigned long bk_cumulative_freq(bktree_s *tree);


/**
 * Add a word to the tree. 
 * @param tree a bktree_s object.
 * @param word the word to be added.
 * @param freq the frequency of the word, NULL can be passed to set it to the 
 * default value.
 * @return true if word was added other wise false. */
bool bk_add_word(bktree_s *tree, const char *word, const unsigned int *freq);



/**
 * Delete a word from the tree.
 * @param tree a bktree_s object.
 * @param word the word to be deleted. 
 * @return true if the word was deleted, false if the word did not exist in the
 * db or there was a problem deleting the word. */
bool bk_delete(bktree_s *tree, const char *word);



/* Definitions for the Character Matrix store. */

/* Private structure to hold the Character Matrix. */
typedef struct cm_s {
    TCHDB *db;
} cm_s;



#define CM_DB_NAME "cm__db"

#define CM_READ 0
#define CM_WRITE 1



/**
 * Loads the Character Matrix.
 * @param access_code either one of CM_READ|CM_WRITE. 
 * @return cm_s or NULL on failure. */
cm_s* cm_init(int access_code);



/**
 * Create a CM store form a file.
 * @param file_name the file that contains the character matrix. It should have
 * the following format:
 *     XY dd.ddd  
 * Where X and Y are characters from the alphabet and dd.ddd is the probability
 * that Y can be misread as X. 
 * @return the number of items added. */
unsigned int cm_create(const char *file_name);



/**
 * Returns the probablity of Y being misread as X where the key is XY. 
 * @param cm the loaded cm_s object.
 * @param key a two character string. */
double cm_get(cm_s *cm, const char *key);



/**
 * Closes and frees the space of the cm_s struct 
 * @param cm the loaded cm_s object. */
void cm_destroy(cm_s *cm);


#endif


#ifdef __cplusplus
} // closing brace for extern "C"

#endif