#ifndef BKCOMMON
#define BKCOMMON_H 1


typedef struct pair_s{
    char *word;
    unsigned int freq;
} pair_s;

/* Frees the pair_s structure allocated by malloc. */
void free_pair_s(void *pair);


/* Converts a string to uppercase. */
char* s_to_upper(const char *word);


// Removes the \n from a string very much like Perl's chomp.
void bk_chomp(char *s);


#endif
